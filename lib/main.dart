import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:places_app/providers/great_places.dart';
import 'package:places_app/screens/places_list_screen.dart';
import 'package:places_app/screens/add_place_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final ThemeData theme = ThemeData();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: GreatPlaces(),
      child: MaterialApp(
        title: 'Great Places',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
          colorScheme: theme.colorScheme.copyWith(secondary: Colors.amber),
        ),
        routes: {
          AddPlaceScreen.routeName: (context) => AddPlaceScreen(),
        },
        home: PlacesListScreen(),
      ),
    );
  }
}
