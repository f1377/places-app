class PlaceLocation {
  final double lactitude;
  final double longitude;
  final String address;

  PlaceLocation({
    required this.lactitude,
    required this.longitude,
    this.address: '',
  });
}
