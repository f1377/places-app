# Places Reminder App [2019]

Build a Place Reminder App [Following Tutorial]

## Dependencies
- [provider](https://pub.dev/packages/provider)
- [image_picker](https://pub.dev/packages/image_picker)
- [path_provider](https://pub.dev/packages/path_provider)
- [path](https://pub.dev/packages/path)
- [sqflite](https://pub.dev/packages/sqflite)
- [location](https://pub.dev/packages/location)

## App Overview

### Places Overview Screen

![Places Overview Screen](/images/readme/places_screen.png "Places Overview Screen")

### Add Place Screen

![Add Place Screen](/images/readme/add_place_screen.png "Add Place Screen")

### Place Added Screen

![Place Added Screen](/images/readme/place_added_screen.png "Place Added Screen")
